# PEINTURE_SYMFONY_PROJECT

PEINTURE_SYMFONY_PROJECT est un site internet présentant des peintures

## Environnement de développement

### Pré-requis

* PHP 7.3.27
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requid (sauf Docker er Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```
#### Configuration de l'environnement
* Pour obtenir la barre de debug lors de l'affichage du site 
=> composer require symfony/profiler-pack

##### Lancer l'environnement de développement

```bash
 docker-compose up -d
 symfony serve -d
```


